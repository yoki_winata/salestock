angular.module('controller', [])
    .controller('mainController', function($scope, $http, Product, CartTxn, OrderTxn, Coupon, Status) {
        $scope.productsArr = {};
        $scope.statusArr = {};
        $scope.orderStatus = {};
        $scope.shippingStatus = {};
        $scope.statusOptions = [];

        Product.getAll()
            .success(function(data) {
                if(data.status_code == 200) {
                    $scope.products = data.product;
                    angular.forEach($scope.products, function(value, key) {
                      // console.log(" key:" + key + " value:" + value);
                      // console.log(value.id);
                      $scope.productsArr[value.id] = value.name;
                    });
                }
                else
                    $scope.products = null;
            });

        Status.getStatus()
            .success(function(data) {
                if(data.status_code == 200) {
                    $scope.status = data.status;
                    angular.forEach($scope.status, function(value, key) {
                      // console.log(" key:" + key + " value:" + value);
                      // console.log(value);
                      $scope.statusArr[key] = value;
                      var temp = {id: key, status: value};
                      $scope.statusOptions.push(temp);
                    });
                }
                else
                    $scope.products = null;
            });


        CartTxn.getAll()
            .success(function(data){
                if(data.status_code == 200)
                    $scope.cartTxns = data.cartTxn;
                else
                    $scope.cartTxns = null;
            });
        OrderTxn.getAll()
            .success(function(response){
                if(response.status_code == 200) {
                    $scope.orderHeaderTxn = response.orderHeaderTxn;
                    $scope.orderDetailTxn = response.orderDetailTxn;
                }
                else {
                    $scope.orderHeaderTxn = null;
                    $scope.orderDetailTxn = null;
                }
            })

        $scope.updatePaymentProof = function() {
          console.log($scope.paymentProof);
          data = {
            "id" : $scope.selectedTxn.id,
            "paymentProof" : $scope.paymentProof
          }
          OrderTxn.updatePaymentProof(data)
            .success(function(res){
                OrderTxn.getAll()
                  .success(function(response){
                      if(response.status_code == 200) {
                          $scope.orderHeaderTxn = response.orderHeaderTxn;
                      }
                      else {
                          $scope.orderHeaderTxn = null;
                      }
                  })
            })
        }

        $scope.addStatus = function(id) {
          OrderTxn.getById(id)
            .success(function(response){
                if(response.status_code == 200) {
                    $scope.adminOrder = response.orderTxn[0];
                    console.log($scope.adminOrder); 
                }
                else {
                    $scope.adminOrder = null;
                }
            })
        }

        $scope.changeOrderStatus = function() {
          data = {
            "id" : $scope.adminOrder.id,
            "status" : $scope.orderStatus.status
          }
          console.log($scope.orderStatus);
          OrderTxn.updateOrderStatus(data)
            .success(function(res){
                OrderTxn.getAll()
                  .success(function(response){
                      if(response.status_code == 200) {
                          $scope.orderHeaderTxn = response.orderHeaderTxn;
                      }
                      else {
                          $scope.orderHeaderTxn = null;
                      }
                  })
            })
        }

        $scope.changeShippingStatus = function() {
          data = {
            "id" : $scope.adminOrder.id,
            "status" : $scope.shippingStatus.status
          }
          OrderTxn.updateShippingStatus(data)
            .success(function(res){
                OrderTxn.getAll()
                  .success(function(response){
                      if(response.status_code == 200) {
                          $scope.orderHeaderTxn = response.orderHeaderTxn;
                      }
                      else {
                          $scope.orderHeaderTxn = null;
                      }
                  })
            })
        }

        $scope.changeShippingId = function() {
          data = {
            "id" : $scope.adminOrder.id,
            "shippingId" : $scope.shippingId
          }
          OrderTxn.updateShippingId(data)
            .success(function(res){
                OrderTxn.getAll()
                  .success(function(response){
                      if(response.status_code == 200) {
                          $scope.orderHeaderTxn = response.orderHeaderTxn;
                      }
                      else {
                          $scope.orderHeaderTxn = null;
                      }
                  })
            })
        }

        $scope.addPaymentProof = function(id) {
          console.log(id);
          OrderTxn.getById(id)
            .success(function(response){
                if(response.status_code == 200) {
                    $scope.selectedTxn = response.orderTxn[0];
                    console.log($scope.selectedTxn);
                }
                else {
                    $scope.selectedTxn = null;
                }
            })
        }

        $scope.submitCartTxn = function() {
            CartTxn.store($scope.selectedProduct, $scope.amount)
                .success(function(data){
                    CartTxn.getAll()
                        .success(function(response){
                            if(response.status_code == 200)
                                $scope.cartTxns = response.cartTxn;
                            else
                                $scope.cartTxns = null;
                        })
                })
                .error(function(data){
                    console.log(data);
                });
        }

        $scope.addCart = function(id) {
            // console.log('addCart('+id+')');
            Product.getById(id)
                .success(function(data) {
                    if(data.status_code == 200) {
                        $scope.selectedProduct = data.product;
                        console.log($scope.selectedProduct);
                    }
                    else
                        $scope.selectedProduct = null;
                });
        }

        $scope.buy = function() {
          console.log($scope.couponId);
          if($scope.couponId == undefined || $scope.couponId == "") {
            OrderTxn.order($scope.couponId)
              .success(function(res) {
                  if(res.status_code == 200) {
                    Coupon.used($scope.couponId)
                      .success(function(resp) {

                      })

                    OrderTxn.getAll()
                      .success(function(response){
                        $scope.errMessage = "";
                            if(response.status_code == 200) {
                                $scope.orderHeaderTxn = response.orderHeaderTxn;
                                $scope.orderDetailTxn = response.orderDetailTxn;
                            }
                            else {
                                $scope.orderHeaderTxn = null;
                                $scope.orderDetailTxn = null;
                            }
                      })

                    Product.getAll()
                      .success(function(data) {
                          if(data.status_code == 200)
                              $scope.products = data.product;
                          else
                              $scope.products = null;
                      });

                    CartTxn.getAll()
                      .success(function(data){
                          if(data.status_code == 200)
                              $scope.cartTxns = data.cartTxn;
                          else
                              $scope.cartTxns = null;
                      });
                  }
              }) 
          } else {
            Coupon.valid($scope.couponId)
              .success(function(data) {
                  if(data.valid) {
                    OrderTxn.order($scope.couponId)
                      .success(function(res) {
                          if(res.status_code == 200) {
                            Coupon.used($scope.couponId)
                              .success(function(resp) {

                              })

                            OrderTxn.getAll()
                              .success(function(response){
                                $scope.errMessage = "";
                                    if(response.status_code == 200) {
                                        $scope.orderHeaderTxn = response.orderHeaderTxn;
                                        $scope.orderDetailTxn = response.orderDetailTxn;
                                    }
                                    else {
                                        $scope.orderHeaderTxn = null;
                                        $scope.orderDetailTxn = null;
                                    }
                              })

                            Product.getAll()
                              .success(function(data) {
                                  if(data.status_code == 200)
                                      $scope.products = data.product;
                                  else
                                      $scope.products = null;
                              });

                            CartTxn.getAll()
                              .success(function(data){
                                  if(data.status_code == 200)
                                      $scope.cartTxns = data.cartTxn;
                                  else
                                      $scope.cartTxns = null;
                              });
                          }
                      }) 
                  } else {
                    $scope.errMessage = "Salah Kupon";
                  }
              })
          }
        }
    })