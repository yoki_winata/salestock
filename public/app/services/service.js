angular.module('service', [])
    .factory('Product', function($http, API_URL) {
        return {
            getAll : function() {
                return $http.get(API_URL + 'product');
            },
            getById : function(id) {
                return $http.get(API_URL + 'product/' + id);
            },
        }
    })
    .factory('Status', function($http, API_URL) {
        return {
            getStatus : function() {
                return $http.get(API_URL + 'status');
            }
        }
    })
    .factory('CartTxn', function($http, API_URL) {
        return {
            getAll : function() {
                return $http.get(API_URL + 'cartTxn');
            },
            store : function(product, amount) {
                cart = {
                    "customerId" : 1,
                    "productId" : product.id,
                    "amount" : amount,
                };
                return $http({
                    method: 'POST',
                    url: API_URL + 'cartTxn',
                    headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(cart)
                });
            },
        }
    })
    .factory('OrderTxn', function($http, API_URL) {
        return {
            getAll : function() {
                return $http.get(API_URL + 'orderTxn');
            },
            getById : function(id) {
                return $http.get(API_URL + 'orderTxn/' + id);
            },
            order : function(couponId) {
                data = {
                    "couponId" : couponId
                }
                return $http({
                    method: 'POST',
                    url: API_URL + 'orderTxn/' + 1,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            },
            updatePaymentProof : function(data) {
                console.log(data.id + data.paymentProof);
                return $http({
                    method: 'POST',
                    url: API_URL + 'orderTxn/paymentProof/' + data.id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            },
            updateOrderStatus : function(data) {
                return $http({
                    method: 'POST',
                    url: API_URL + 'orderTxn/orderStatus/' + data.id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            },
            updateShippingStatus : function(data) {
                return $http({
                    method: 'POST',
                    url: API_URL + 'orderTxn/shippingStatus/' + data.id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            },
            updateShippingId : function(data) {
                return $http({
                    method: 'POST',
                    url: API_URL + 'orderTxn/shippingId/' + data.id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            }
        }
    })
    .factory('Coupon', function($http, API_URL) {
        return {
            valid : function(id) {
                return $http.get(API_URL + 'coupon/valid/' + id);
            },
            used : function(id) {
                data = {
                    "couponId" : id
                }
                return $http({
                    method: 'POST',
                    url: API_URL + 'coupon/used/' + id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                })
            }
        }
    });