<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('index');
});

Route::group(array('prefix' => 'api'), function() {
	Route::resource('product', 'MsProductController');
	Route::resource('coupon', 'MsCouponController');
	Route::get('coupon/valid/{id}', 'MsCouponController@isValid');
	Route::post('coupon/used/{id}', 'MsCouponController@used');
	Route::resource('cartTxn', 'CartTxnController');
	Route::resource('orderTxn', 'OrderTxnController');
	Route::post('orderTxn/{id}', 'OrderTxnController@store');
	Route::post('orderTxn/paymentProof/{id}', 'OrderTxnController@updatePaymentProof');
	Route::post('orderTxn/orderStatus/{id}', 'OrderTxnController@updateOrderStatus');
	Route::post('orderTxn/shippingStatus/{id}', 'OrderTxnController@updateShippingStatus');
	Route::post('orderTxn/shippingId/{id}', 'OrderTxnController@updateShippingId');
	Route::resource('customer', 'MsCustomerController');
	Route::get('status', 'StatusController@index');
});
