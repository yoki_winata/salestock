<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\MsCoupon;
use Carbon\Carbon;

class MsCouponController extends Controller
{
    protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupon = MsCoupon::getAll();
        if($coupon == null) {
            return $this->errResponse;
        }

        $this->response['coupon'] = $coupon;
        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function isValid($id) {
        $coupon = MsCoupon::searchById($id);
        if($coupon == null) {
            return $this->errResponse;
        }

        $now = Carbon::today();
        $validFrom = new Carbon($coupon->validFrom);
        $validTo = new Carbon($coupon->validTo);

        $this->response['valid'] = ($now >= $validFrom && $now <= $validTo);
        return $this->response;
    }

    public function used($id) {
        $coupon = MsCoupon::find($id);
        if($coupon == null) {
            return $this->errResponse;
        }

        $coupon->stock -= 1;
        $result = $coupon->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }
}
