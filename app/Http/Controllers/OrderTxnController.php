<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\OrderHeaderTxn;
use App\Model\OrderDetailTxn;
use App\Model\CartTxn;
use App\Model\MsProduct;
use App\Model\Constant;
use Carbon\Carbon;

class OrderTxnController extends Controller
{
    protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderHeaderTxn = OrderHeaderTxn::getAll();
        $orderDetailTxn = OrderDetailTxn::getAll();
        if($orderHeaderTxn == null || $orderDetailTxn == null) {
            return $this->errResponse;
        }

        $this->response['orderHeaderTxn'] = $orderHeaderTxn;
        $this->response['orderDetailTxn'] = $orderDetailTxn;
        return $this->response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $customerId)
    {
        $cartTxn = CartTxn::searchByCustomer($customerId);
        if($cartTxn == null) return $this->errResponse;

        $sign = false;
        foreach($cartTxn as $value) {
            if(!MsProduct::checkStock($value->productId, $value->amount)) {
                $this->errResponse[$value->productId] = false;
                $sign = true; 
            }
        }

        if($sign == true) {
            $product = MsProduct::all();
            if($product == null) {
                $this->errResponse['product'] = false;
            }
            $this->errResponse['product'] = $product;
            return $this->errResponse;
        }

        $txnId = Carbon::now();
        $txnId = str_replace(['-',' ',':'], ['','',''], $txnId);

        $orderHeaderTxn = new OrderHeaderTxn();
        $orderHeaderTxn->id = $txnId;
        $orderHeaderTxn->customerId = $customerId;
        $orderHeaderTxn->couponId   = $request->couponId;
        $orderHeaderTxn->orderStatus = Constant::Pending;
        $orderHeaderTxn->save();        
        
        foreach($cartTxn as $value) {
            $orderDetailTxn = new OrderDetailTxn();
            $orderDetailTxn->id = $txnId;
            $orderDetailTxn->productId  = $value->productId;
            $orderDetailTxn->amount     = $value->amount;
            $result = $orderDetailTxn->save();
            if($result != true) {
                $this->errResponse['result'] = $result;
                return $this->errResponse;
            }

            $product = MsProduct::find($value->productId);
            if($product == null) {
                return $this->errResponse;
            }

            $product->stock -= $value->amount;
            $result = $product->save();
            if($result != true) {
                $this->errResponse['result'] = $result;
                return $this->errResponse;
            }
        }

        CartTxn::deleteByCustomer($customerId);  
        
        return $this->response;
    }

    public function updatePaymentProof(Request $request, $id) {
        $orderHeaderTxn = OrderHeaderTxn::find($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $orderHeaderTxn->paymentProof = $request->paymentProof;
        $result = $orderHeaderTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function updateOrderStatus(Request $request, $id) {
        $orderHeaderTxn = OrderHeaderTxn::find($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $orderHeaderTxn->orderStatus = Constant::getValue($request->status);
        $result = $orderHeaderTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function updateShippingStatus(Request $request, $id) {
        $orderHeaderTxn = OrderHeaderTxn::find($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $orderHeaderTxn->shippingStatus = Constant::getValue($request->status);
        $result = $orderHeaderTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function updateShippingId(Request $request, $id) {
        $orderHeaderTxn = OrderHeaderTxn::find($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $orderHeaderTxn->shippingId = $request->shippingId;
        $result = $orderHeaderTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function updateLogisticPartner($id, $logisticPartner) {
        $orderHeaderTxn = OrderHeaderTxn::find($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $orderHeaderTxn->logisticPartner = $logisticPartner;
        $result = $orderHeaderTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function getShippingStatus($id) {
        $orderHeaderTxn = OrderHeaderTxn::getByShippingId($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $this->response['shippingStatus'] = Constant::getKey($orderHeaderTxn->shippingStatus);

        return $this->response;
    }

    public function getOrderStatus($id) {
        $orderHeaderTxn = OrderHeaderTxn::getById($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $this->response['shippingStatus'] = Constant::getKey($orderHeaderTxn->orderStatus);

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderHeaderTxn = OrderHeaderTxn::getById($id);
        if($orderHeaderTxn == null) {
            return $this->errResponse;
        }

        $this->response['orderTxn'] = $orderHeaderTxn;
        return $this->response;
    }
}
