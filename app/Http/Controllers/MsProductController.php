<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\MsProduct;
use App\Model\Constant;

class MsProductController extends Controller
{
    protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = MsProduct::all();
        if($product == null) {
            return $this->errResponse;
        }
        
        $this->response['product'] = $product;
        return $this->response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = MsProduct::find($id);
        if($product == null) {
            return $this->errResponse;
        }

        $product->stock -= 1;
        $result = $product->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;
        }

        return $this->response;
    }

    public function show($id) {
        $product = MsProduct::find($id);
        if($product == null) {
            return $this->errResponse;
        }

        $this->response['product'] = $product;
        return $this->response;
    }
    
}
