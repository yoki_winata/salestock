<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\Constant;

class StatusController extends Controller
{
		protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];

    public function index() {
    	$status = Constant::getKeys();
    	if($status == null) {
         return $this->errResponse;
      }

      $this->response['status'] = $status;
      return $this->response;
    }
}
