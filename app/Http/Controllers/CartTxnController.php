<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\CartTxn;

class CartTxnController extends Controller
{
    protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartTxn = CartTxn::all();
        if($cartTxn == null) {
            return $this->errResponse;
        }

        $this->response['cartTxn'] = $cartTxn;
        return $this->response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cartTxn = new CartTxn();
        $cartTxn->customerId = $request->customerId;
        $cartTxn->productId  = $request->productId;
        $cartTxn->amount    = $request->amount;
        $result = $cartTxn->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;           
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cartTxn = CartTxn::searchByCustomer($id);
        $this->response['cartTxn'] = $cartTxn;
        return $this->response;
    }
}
