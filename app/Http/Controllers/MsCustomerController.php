<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\MsCustomer;

class MsCustomerController extends Controller
{
    protected $response = ['status_code' => 200];
    protected $errResponse = ['status_code' => 404];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = MsCustomer::all();
        if($customer == null) {
            return $this->errResponse;
        }

        $this->response['customer'] = $customer;
        return $this->response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new MsCustomer();
        $customer->name         = $request->name;
        $customer->phone_number = $request->phone_number;
        $customer->email        = $request->email;
        $customer->address      = $request->address;
        $result = $customer->save();
        if($result != true) {
            $this->errResponse['result'] = $result;
            return $this->errResponse;           
        }

        return $this->response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
