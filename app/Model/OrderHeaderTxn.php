<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderHeaderTxn extends Model
{
    protected $table = 'OrderHeaderTxn';

    public static function getAll() {
    	return DB::table('OrderHeaderTxn')->get();
    }

    public static function getByShippingId($id) {
    	return OrderHeaderTxn::where('shippingId', $id)
    					       ->first();
    }

    public static function getById($id) {
    	return DB::table('OrderHeaderTxn')
                    ->where('id', '=', $id)
    			    ->get();
    }
}
