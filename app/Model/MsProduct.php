<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MsProduct extends Model
{
    protected $table = 'MsProduct';

    public static function checkStock($id, $amount) {
    	$product = MsProduct::where('id', $id)
    												->first();
    	return ($product->stock >= $amount);
    }
}
