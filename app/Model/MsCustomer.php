<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MsCustomer extends Model
{
    protected $table = 'MsCustomer';
}
