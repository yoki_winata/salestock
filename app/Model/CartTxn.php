<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartTxn extends Model
{	
		use SoftDeletes;
		
		protected $dates = ['deleted_at'];
    protected $table = 'CartTxn';

    public static function searchByCustomer($id) {
    	return CartTxn::where('customerId', $id)
                      ->where('deleted_at', NULL)
                      ->get();
    }

    public static function deleteByCustomer($id) {
    	return CartTxn::where('customerId', $id)
    									->delete();
    }
}
