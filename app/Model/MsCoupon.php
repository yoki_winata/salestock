<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class MsCoupon extends Model
{
    protected $table = 'MsCoupon';

    public static function searchById($id) {
    	return DB::table('MsCoupon')
                ->where('id', '=', $id)
                ->first();
    }

    public static function getAll() {
    	return DB::table('MsCoupon')->get();
    }
}
