<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderDetailTxn extends Model
{
    protected $table = 'OrderDetailTxn';

    public static function getAll() {
    	return DB::table('OrderDetailTxn')->get();
    }
}
