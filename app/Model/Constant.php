<?php

namespace App\Model;
use ReflectionClass;

class Constant extends BasicEnum
{
    const Pending = 1;
    const Processed = 2;
    const Cancelled = 3;
    const OnDelivery = 4;
    const Delivered = 5;

    private static $keys = NULL;
    private static $values = NULL;

    public static function getKeys() {
    	if(self::$keys == NULL) {
    		$obj = self::getConstants();
	    	while ($o = current($obj)) {
				  self::$keys[$obj[key($obj)]] = key($obj);
				  next($obj);
				}
    	}
    	return self::$keys;
    }

    public static function getValues() {
    	if(self::$values == NULL) {
    		$obj = self::getConstants();
	    	while ($o = current($obj)) {
				  self::$values[strtolower(key($obj))] = $obj[key($obj)];
				  next($obj);
				}
    	}
    	return self::$values;
    }

    public static function getKey($value) {
    	// print_r(self::getKeys());
    	return self::getKeys()[$value];
    }

    public static function getValue($key) {
    	return self::getValues()[strtolower($key)];
    }
}

class BasicEnum {
	private static $constCacheArray = NULL;

	private function __construct() {

	}

	public static function getConstants() {
    if (self::$constCacheArray == NULL) {
        self::$constCacheArray = [];
    }
    $calledClass = get_called_class();
    if (!array_key_exists($calledClass, self::$constCacheArray)) {
        $reflect = new ReflectionClass($calledClass);
        self::$constCacheArray[$calledClass] = $reflect->getConstants();
    }
    return self::$constCacheArray[$calledClass];
  }

  public static function isValidName($name, $strict = false) {
    $constants = self::getConstants();

    if ($strict) {
        return array_key_exists($name, $constants);
    }

    $keys = array_map('strtolower', array_keys($constants));
    return in_array(strtolower($name), $keys);
  }

  public static function isValidValue($value, $strict = true) {
    $values = array_values(self::getConstants());
    return in_array($value, $values, $strict);
  }
}
