<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MsCoupon', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name');
            $table->float('amount');
            $table->integer('denom');
            $table->integer('stock');
            $table->dateTime('validFrom');
            $table->dateTime('validTo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('MsCoupon');
    }
}
