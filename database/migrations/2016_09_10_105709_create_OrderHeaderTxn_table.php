<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHeaderTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OrderHeaderTxn', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->integer('customerId');
            $table->string('couponId');
            $table->string('paymentProof');
            $table->integer('orderStatus');
            $table->string('logisticPartner');
            $table->string('shippingId');
            $table->integer('shippingStatus');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('OrderHeaderTxn');
    }
}
