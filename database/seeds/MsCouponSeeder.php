<?php

use Illuminate\Database\Seeder;

class MsCouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MsCoupon')->delete();

        DB::table('MsCoupon')->insert([
        	'id' 		=> 'ID25000' ,
        	'name' 		=> 'Diskon 25000',
        	'amount' 	=> 25000,
        	'denom'		=> 1,
        	'stock' 	=> 20,
        	'validFrom' => '2016-09-10 00:00:00',
        	'validTo' 	=> '2016-09-20 00:00:00'
        ]);

        DB::table('MsCoupon')->insert([
        	'id' 		=> 'ID25P' ,
        	'name' 		=> 'Diskon 25%',
        	'amount' 	=> 25,
        	'denom'		=> 2,
        	'stock' 	=> 10,
        	'validFrom' => '2016-09-10 00:00:00',
        	'validTo' 	=> '2016-09-30 00:00:00'
        ]);

        DB::table('MsCoupon')->insert([
        	'id' 		=> 'ID50P' ,
        	'name' 		=> 'Diskon 50%',
        	'amount' 	=> 50,
        	'denom'		=> 2,
        	'stock' 	=> 5,
        	'validFrom' => '2016-09-10 00:00:00',
        	'validTo' 	=> '2016-09-18 00:00:00'
        ]);
    }
}
