<?php

use Illuminate\Database\Seeder;

class MsProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('MsProduct')->delete();

        DB::table('MsProduct')->insert([
        	'name' 	=> 'Baju',
        	'price' => 100000,
        	'stock' => 20
        ]);

        DB::table('MsProduct')->insert([
        	'name' 	=> 'Sepatu',
        	'price' => 150000,
        	'stock' => 30
        ]);

        DB::table('MsProduct')->insert([
        	'name' 	=> 'Celana',
        	'price' => 125000,
        	'stock' => 25
        ]);
    }
}
