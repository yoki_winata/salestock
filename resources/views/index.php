<!DOCTYPE html>
<html>
  <head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="app/controllers/controller.js"></script>
    <script src="app/services/service.js"></script>
    <script src="app/app.js"></script>
  </head>

  <body ng-app="mainApp" ng-controller="mainController">
    <div class="container">
      <h1>Pelanggan</h1>
      <h3>Barang Dagangan</h3>
      <div class="content" ng-repeat="product in products">
        Nama: {{ product.name }} - Harga: Rp. {{ product.price }} - Tersedia: {{ product.stock }}
        <button ng-click="addCart(product.id)">Lihat Detail</button>
      </div>

      <h3>Detail Barang</h3>
      <div class="content">
        <form ng-submit="submitCartTxn()">
          Nama: {{ selectedProduct.name }} - Harga: Rp. {{ selectedProduct.price }} - Tersedia: {{ selectedProduct.stock }}
          <input type="text" name="amount" ng-model="amount">
          <button type="submit">Tambah Ke Keranjang</button>
        </form>
      </div>

      <h3>Keranjang Belanja</h3>
      <div class="content" ng-repeat="cartTxn in cartTxns">
        Nama: {{ productsArr[cartTxn.productId] }} - Jumlah: {{ cartTxn.amount }}
      </div>

      <label>Kupon: </label><input type="text" name="couponId" ng-model="couponId">
      <label style="color: red">{{ errMessage }}</label>
      <button ng-click="buy()">Beli</button>

      <h3>Daftar Pesanan</h3>
      <div class="content" ng-repeat="headerTxn in orderHeaderTxn">
        TxnId: {{ headerTxn.id }} - Coupon: {{ headerTxn.couponId }} - Status Pesanan: {{ statusArr[headerTxn.orderStatus] }} - Logistic: {{ headerTxn.logisticPartner }} - Kode Shipping: {{ headerTxn.shippingId }} - Status Shipping: {{ statusArr[headerTxn.shippingStatus] }} - Payment Proof: {{ headerTxn.paymentProof }} 
        <button ng-click="addPaymentProof(headerTxn.id)">Ubah Payment Proof</button>
        <div ng-repeat="detailTxn in orderDetailTxn" ng-if="headerTxn.id == detailTxn.id">
          Name: {{ productsArr[detailTxn.productId] }} - Jumlah: {{ detailTxn.amount }}
        </div>
      </div>

      <h3>Ubah Payment Proof</h3>
      <div class="content">
        TxnId: {{ selectedTxn.id }} - Coupon: {{ selectedTxn.couponId }} - Status Pesanan: {{ selectedTxn.orderStatus }} - Logistic: {{ selectedTxn.logisticPartner }} - Kode Shipping: {{ selectedTxn.shippingId }} - Status Shipping: {{ selectedTxn.shippingStatus }} - Payment Proof: {{ selectedTxn.paymentProof }}
        <input type="text" name="paymentProof" ng-model="paymentProof">
        <button ng-click="updatePaymentProof()">Ubah</button>
      </div>


      <h1>Admin</h1>
      <h3>Daftar Pesanan</h3>
      <div class="content" ng-repeat="headerTxn in orderHeaderTxn">
        TxnId: {{ headerTxn.id }} - Coupon: {{ headerTxn.couponId }} - Status Pesanan: {{ statusArr[headerTxn.orderStatus] }} - Logistic: {{ headerTxn.logisticPartner }} - Kode Shipping: {{ headerTxn.shippingId }} - Status Shipping: {{ statusArr[headerTxn.shippingStatus] }} - Payment Proof: {{ headerTxn.paymentProof }} 
        <button ng-click="addStatus(headerTxn.id)">Ubah Status Order</button>
        <button ng-click="addStatus(headerTxn.id)">Ubah Status Shipping</button>
        <div ng-repeat="detailTxn in orderDetailTxn" ng-if="headerTxn.id == detailTxn.id">
          Name: {{ productsArr[detailTxn.productId] }} - Jumlah: {{ detailTxn.amount }}
        </div>
      </div>

      <h3>Ubah Status Order</h3>
      <div class="content">
        TxnId: {{ adminOrder.id }} - Coupon: {{ adminOrder.couponId }} - Status Pesanan: {{ statusArr[adminOrder.orderStatus] }}
        <select ng-model="orderStatus"
          ng-options="p as (p.status) for p in statusOptions">
        </select>
        <button ng-click="changeOrderStatus()">Ubah Status Order</button>
        - Logistic: {{ adminOrder.logisticPartner }} - Kode Shipping: {{ adminOrder.shippingId }} - Status Shipping: {{ statusArr[adminOrder.shippingStatus] }} - Payment Proof: {{ adminOrder.paymentProof }} 
      </div>

      <h3>Ubah Status Shipping</h3>
      <div class="content">
        TxnId: {{ adminOrder.id }} - Coupon: {{ adminOrder.couponId }} - Status Pesanan: {{ statusArr[adminOrder.orderStatus] }} - Logistic: {{ adminOrder.logisticPartner }} - Kode Shipping: {{ adminOrder.shippingId }} - Status Shipping: {{ statusArr[adminOrder.shippingStatus] }} 
        <select ng-model="shippingStatus"
          ng-options="p as (p.status) for p in statusOptions">
        </select>
        <button ng-click="changeShippingStatus()">Ubah Status Shipping</button>
        - Payment Proof: {{ adminOrder.paymentProof }}
      </div>

      <h3>Ubah Shipping ID</h3>
      <div class="content">
        TxnId: {{ adminOrder.id }} - Coupon: {{ adminOrder.couponId }} - Status Pesanan: {{ statusArr[adminOrder.orderStatus] }} - Logistic: {{ adminOrder.logisticPartner }} - Kode Shipping: {{ adminOrder.shippingId }}
        <input type="text" name="shippingId" ng-model="shippingId">
        <button ng-click="changeShippingId()">Ubah Shipping ID</button>
        - Status Shipping: {{ statusArr[adminOrder.shippingStatus] }} - Payment Proof: {{ adminOrder.paymentProof }} 
      </div>
    </div>
  </body>
</html>
